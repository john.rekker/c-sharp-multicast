﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace ConsoleSocketMulticast
{
	class Program
	{
		static void Main(string[] args)
		{
			var s			= new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
			IPEndPoint ipep= new IPEndPoint(IPAddress.Any, 7777);
			s.Bind(ipep);


			IPAddress ip	= IPAddress.Parse("224.5.6.8");

			

			s.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.AddMembership, new MulticastOption(ip));
			

			

			byte[] b = new byte[10];
			b[0]	 = (byte)'H';
			b[1]	 = (byte)'e';
			b[2]	 = (byte)'l';
			b[3]	 = (byte)'l';
			b[4]	 = (byte)'o';

			//s.Send(b, 5, SocketFlags.None);
			s.Receive(b, 10, SocketFlags.None);

		}
	}
}
